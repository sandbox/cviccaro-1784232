(function($) {
	Drupal.behaviors.recaptcha_modal = {
		attach:function(context, settings) {
			var KEYCODE_ESCAPE = 27;
			if (Drupal.settings.recaptcha_modal) {
				var passed = false;
				var id = Drupal.settings.recaptcha_modal.id;
				var public_key = Drupal.settings.recaptcha_modal.public_key;
				var $captcha = $("#"+id,context);
				if ($captcha.length) {
						var $container = $("#container").length ? $("#container") : $("body");
						
						
						var $parent = $captcha.parent();
						var $form = $('form.recaptcha-modal',context);
						$form.attr('action','system/ajax');
						var redirect = Drupal.settings.recaptcha_modal.redirect_url;
						var $wrapper = $("#recaptcha_modal_wrapper").length ? $("#recaptcha_modal_wrapper") : $('<div id="recaptcha_modal_wrapper" class="recaptcha-modal-wrapper" />');
						var $closeX = $(".recaptcha-modal-close",$wrapper).length ? $(".recaptcha-modal-close",$wrapper) : $('<div class="captcha-close recaptcha-modal-close" />');
						var $submitCaptcha = $('<button class="captcha-submit recaptcha-modal-submit">OK</button>');
						var $feedback = $('<div class="captcha-feedback recaptcha-modal-feedback" />');
						var $modal_response = $form.find('[name="recaptcha_modal_response"]');
						var $modal_challenge = $form.find('[name="recaptcha_modal_challenge"]');
						var $actions = $form.find('.form-actions');
						//Set height
						$wrapper.height($(document).height());
						//Hide form actions
						$actions.hide();
						$captcha.attr('id','recaptcha_ajax_api_container');
						if (!$("#recaptcha_modal_wrapper").length) {
							$container.append($wrapper);	
						}
						$wrapper.html($captcha);
						var recaptcha_feedback = function(msg) {
							var error = arguments.length === 2 ? arguments[1] : null;
							if (error) {
								$captcha.addClass(error);
							}	
							$feedback.html(msg).fadeIn(250);
							if (typeof Modernizr != "undefined" && Modernizr.cssanimations) {
								$captcha.addClass('animated shake');
								setTimeout(function() {
									$captcha.removeClass('animated').removeClass('shake');
								},1250);
							}
							setTimeout(function() {
								$feedback.fadeOut(250);
								if (error) {
									$captcha.removeClass(error);
								}
							},5000);
						}
						if (!$captcha.hasClass('recaptcha-modal-processed')) {
							$captcha.addClass('recaptcha-modal-processed');
							//console.log('creating recaptcha on #recaptcha_ajax_api_container');
							Recaptcha.create(public_key,'recaptcha_ajax_api_container',{theme:Drupal.settings.recaptcha_modal.theme,callback:function() { 
								//$captcha.position({'my':'center','at':'center','of':$wrapper});
								//Prepend and bind the close controls
								$modal_challenge.val(Recaptcha.get_challenge());
								$closeX.click(function() {
									$wrapper.fadeOut(250);
								});
								$submitCaptcha.click(function() {
									$modal_response.val(Recaptcha.get_response());
									$wrapper.fadeOut(250,function() {
										$faux.hide();
										$actions.show();
										$form.submit();
									});
								});
								$(window).keyup(function(e) {
									if (e.keyCode == KEYCODE_ESCAPE) {
										$closeX.click();
									}
								})
								$captcha.prepend($closeX).append($feedback,$submitCaptcha);
								//Set up position handling
								var positionBox = function() {
									var offset = (($(window).height() - $captcha.height()) / 2) + window.pageYOffset;
									$captcha.css({'position':'relative','top':offset});	
								}
								positionBox();
								$(window).scroll(positionBox); 
								var $submit = $form.find('.form-submit');
								var thisAjax = Drupal.ajax[$submit.attr('id')];
								var $faux = $form.find('.recaptcha-faux-submit');
								$form.ajaxForm({
									dataType:'json',
									beforeSerialize:function($form,options) {
										//console.log(arguments);
										if (Recaptcha.get_response().length === 0) {
											$wrapper.fadeIn(250);
											$form.find('.messages error').remove();
											return false;
										}
									},
									beforeSubmit:function(arr,$form,options) {
										$modal_response.val(Recaptcha.get_response());
										//console.log(arguments);
									},
									//url:'recaptcha_modal/ajax',
									success:function(response, status) {
										var failed = false;
										for (var i in response) {
											//console.log(response[i],thisAjax.commands[response[i]['command']]);
											if (response[i]['command'] && thisAjax.commands[response[i]['command']]) {
												if (response[i].failed_captcha) {
													//console.log('FAILED CAPTCHA');
													failed = true;
													$actions.hide();
													$faux.show();
													$captcha.find('.recaptcha_input_area > input').val('');
													$modal_response.val('');
													//scroll to top
													$("html,body").animate({scrollTop:0});
												}
												thisAjax.commands[response[i]['command']](thisAjax,response[i], status);
											}
										}
									  // Reattach behaviors, if they were detached in beforeSerialize(). The
									  // attachBehaviors() called on the new content from processing the response
									  // commands is not sufficient, because behaviors from the entire form need
									  // to be reattached.
									  if (thisAjax.form) {
									    var settings = thisAjax.settings || Drupal.settings;
									    Drupal.attachBehaviors(thisAjax.form, settings);
									  }
									  if (!failed) {
											if (redirect.length) {
												window.location.href = redirect;
											}
											else {
												alert('Thank you!  Your message has been sent.');
												$form.resetForm();
											}
									  }
									},
									error:function() {
										console.log('error',arguments);
									}
								});
							}
						});
					}
					else {
						
					}
				}
			}
		}
	}
})(jQuery);
