<?php

/**
 * @file
 * Provides the reCAPTCHA Modal administration settings.
 */

/**
 * Form callback; administrative settings for reCaptcha.
 */
function recaptcha_modal_admin_settings() {
  // Load the recaptcha library.
  _recaptcha_modal_load_library();

  $form = array();
  $form['recaptcha_modal_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Key'),
    '#default_value' => variable_get('recaptcha_modal_public_key', ''),
    '#maxlength' => 40,
    '#description' => t('The public key given to you when you <a href="@url" target="_blank">registered at reCAPTCHA.net</a>.', array('@url' => url(recaptcha_get_signup_url($_SERVER['SERVER_NAME'], variable_get('site_name', ''))))),
    '#required' => TRUE,
  );
	$private_key = variable_get('recaptcha_modal_private_key','');
  $form['recaptcha_modal_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key'),
    '#default_value' => $private_key,
    '#maxlength' => 40,
    '#description' => t('The private key given to you when you <a href="@url" target="_blank">registered at reCAPTCHA.net</a>.', array('@url' => url(recaptcha_get_signup_url($_SERVER['SERVER_NAME'], variable_get('site_name', ''))))),
    '#required' => TRUE,
  );
	if ($private_key) {
		file_put_contents(drupal_get_path('module','recaptcha_modal') . '/include/recaptcha.key',$private_key);
	}
  $form['recaptcha_modal_secure_connection'] = array(
    '#type' => 'checkbox',
    '#title' => t('Secure Connection'),
    '#default_value' => variable_get('recaptcha_modal_secure_connection', FALSE),
    '#description' => t('Connect to the reCAPTCHA server using a secure connection.'),
  );
  $form['recaptcha_modal_ajax_api'] = array(
    '#type' => 'checkbox',
    '#title' => t('AJAX API'),
    '#default_value' => variable_get('recaptcha_modal_ajax_api', FALSE),
    '#description' => t('Use the AJAX API to display reCAPTCHA.'),
  );
  $form['recaptcha_modal_theme_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['recaptcha_modal_theme_settings']['recaptcha_modal_theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#description' => t('Defines which theme to use for reCAPTCHA.'),
    '#options' => array(
      'red' => t('Red'),
      'white' => t('White'),
      'blackglass' => t('Black Glass'),
      'clean' => t('Clean'),
      'custom' => t('Custom'),
    ),
    '#default_value' => variable_get('recaptcha_modal_theme', 'red'),
    '#required' => TRUE,
  );
  $form['recaptcha_modal_theme_settings']['recaptcha_modal_tabindex'] = array(
    '#type' => 'textfield',
    '#title' => t('Tab Index'),
    '#description' => t('Sets a <a href="@tabindex" target="_blank">tabindex</a> for the reCAPTCHA text box. If other elements in the form use a tabindex, this should be set so that navigation is easier for the user.', array('@tabindex' => 'http://www.w3.org/TR/html4/interact/forms.html#adef-tabindex')),
    '#default_value' => variable_get('recaptcha_modal_tabindex', ''),
    '#size' => 4,
  );

  return system_settings_form($form);
}

/**
 * Validation function for recaptcha_modal_admin_settings().
 *
 * @see recaptcha_modal_admin_settings()
 */
function recaptcha_modal_admin_settings_validate($form, &$form_state) {
  $tabindex = $form_state['values']['recaptcha_modal_tabindex'];
  if (!empty($tabindex) && !is_numeric($tabindex)) {
    form_set_error('recaptcha_modal_tabindex', t('The Tab Index must be an integer.'));
  }
}
